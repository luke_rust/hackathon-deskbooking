::Removes any existing virtual environment
rmdir .venv /s /q
::Create virtual environment
python -m venv .\.venv 
::Activates the virtual environment and installs libraries specified in requirements.txt
.venv\Scripts\activate & pip install -r requirements.txt 