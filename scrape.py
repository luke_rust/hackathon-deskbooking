import argparse
import logging
import sys
import login_config
import time

from traceback import extract_tb
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

log = logging.getLogger(__name__)

def main():
    parser = argparse.ArgumentParser(prog='deskbooking_scraper', description='Scrapes the desk booking page')
    parser.add_argument('--debug', action='store_true', help='debug mode')
    parser.add_argument('--start_date', help='optional start date to override default')
    
    args = parser.parse_args()

    log_handler = logging.StreamHandler(sys.stdout)
    log_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s'))

    if args.debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    log.addHandler(log_handler)

    try:        
        scrape_page('Pegasus', 'CTM')

    except Exception as ex:
        log.error("line {} in main - {} - {}".format(extract_tb(sys.exc_info()[2])[0][1], str(type(ex)),str(ex)))


def scrape_page(selected_location, selected_group):
    try:
        driver = webdriver.Chrome()

        #Click the login button
        driver.get("https://bglgroup.condecosoftware.com/Master.aspx")
        elem = driver.find_element_by_name("btnRedirectID")
        elem.click()

        #Give the page time to render
        time.sleep(2)

        #Enter in the username
        elem = driver.find_element_by_name("loginfmt")
        elem.send_keys(login_config.email, Keys.RETURN)

        #Give the page time to render
        time.sleep(10)

        driver.find_elements_by_class_name("nav-link")[1].click()

        #Give the page time to render
        time.sleep(2)

        #Jump into the iframe, so we can interact with the elements
        driver.switch_to.frame(driver.find_element_by_id("leftNavigation"))

        elem = driver.find_element_by_id('A14')
        elem.click()

        #Give the page time to render
        time.sleep(5)

        log.info("test")

        driver.switch_to.default_content()

        #Jump into the iframe containing the grid
        driver.switch_to.frame(driver.find_element_by_id('mainDisplayFrame'))

        if selected_location == 'Pegasus':
            desk_offset_num = 890
        elif selected_location == 'Bretton':
            desk_offset_num = 307

        grid_dict = dict()

        for booked_slot in driver.find_elements_by_class_name("deskbooking-event"):
            log.info(booked_slot)
            slot_location = "{}-{}".format(booked_slot.location['x'], booked_slot.location['y'])
            grid_dict[slot_location] = dict()
            grid_dict[slot_location]['name'] = booked_slot.text

        for booking_slot in driver.find_elements_by_class_name("desk-roomslot-action"):
            log.info(booking_slot)
            slot_location = "{}-{}".format(booking_slot.location['x'], booking_slot.location['y'])
            if slot_location in grid_dict:
                grid_dict[slot_location]['date'] = booking_slot.get_attribute('data-date')
                grid_dict[slot_location]['desk'] = int(booking_slot.get_attribute('data-slot-room')) - desk_offset_num

            slot_location = "{}-{}".format(booking_slot.location['x'], booking_slot.location['y'] - 1)
            if slot_location in grid_dict:
                grid_dict[slot_location]['date'] = booking_slot.get_attribute('data-date')
                grid_dict[slot_location]['desk'] = int(booking_slot.get_attribute('data-slot-room')) - desk_offset_num

            slot_location = "{}-{}".format(booking_slot.location['x'], booking_slot.location['y'] + 1)
            if slot_location in grid_dict:
                grid_dict[slot_location]['date'] = booking_slot.get_attribute('data-date')
                grid_dict[slot_location]['desk'] = int(booking_slot.get_attribute('data-slot-room')) - desk_offset_num

        return grid_dict

    except Exception as ex:
        log.error("line {} in scrape_page - {} - {}".format(extract_tb(sys.exc_info()[2])[0][1], str(type(ex)),str(ex)))

if __name__ == '__main__':
    main()